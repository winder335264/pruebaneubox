<?php

    /* 
        Winder Morillo 
        Fecha: 04/11/2020
        Problema Nro 1 Codificado en PHP (Lenguaje con mayor dominio)
    */

    
    //----- INICIO PROGRAMA PRINCIPAL ----- 

    $archivoEntrada = file("entrada.txt");                                          //Leer archivo entrada
    $resultadoPrimeraInstruccion = descifrarInstruccion($archivoEntrada, 1);        //Llamada para decifrar las instruccione 1
    $resultadoSegundaInstruccion = descifrarInstruccion($archivoEntrada, 2);        //Llamada para decifrar las instruccione 1
    escribirSalida($resultadoPrimeraInstruccion,$resultadoSegundaInstruccion);      //Llamada a función que escribe la salida de los resultados

    //-----FIN PROGRAMA PRINCIPAL ----- 


    function descifrarInstruccion($entrada, $posicion)
    {

        $intruccion     = str_replace(' ', '', $entrada[$posicion]);     //Se lee la instruccion
        $mensaje        = str_replace(' ', '', $entrada[3]);             //Se lee el mensaje
        $arrMensaje     = str_split($mensaje);
        $nuevoMensaje   = '';
        $letraAnt       = '';

        for ($i=0; $i < count($arrMensaje); $i++) 
        {
            if($i == 0)
            {
                $nuevoMensaje = $arrMensaje[$i];
            }
            else
            {
                if (strcmp($letraAnt, $arrMensaje[$i]) != 0)
                {
                    $nuevoMensaje .= $arrMensaje[$i]; //Se construye un nuevo mensaje limpiando los caracteres repetidos
                }
            }

            $letraAnt= $arrMensaje[$i]; 
        }

        if (preg_match ('/[a-zA-Z0-9]/', $nuevoMensaje)) //Se valida caracteres alfanumericos 
        {
            
            $pos = strpos(trim($nuevoMensaje), trim($intruccion)); //Se valida que la instruccion si se encuentre en el mensaje limpio

            if ($pos === false)
            {
                $existe = 'NO'; 
            } 
            else 
            {
                $existe = 'SI'; 
            }

        }
        else
        {
            $existe = 'NO';
        }
        
        return $existe;

    }

    function escribirSalida($resultadoPrimeraInstruccion, $resultadoSegundaInstruccion)
    {
        $escribirArchivo = fopen("salida.txt", "w");

        if($escribirArchivo)
        {
            fwrite($escribirArchivo, $resultadoPrimeraInstruccion.PHP_EOL);
            fwrite($escribirArchivo, $resultadoSegundaInstruccion.PHP_EOL);
        }
    
        fclose($escribirArchivo);

    }
    

?>