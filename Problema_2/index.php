<?php

    /*
        Winder Morillo
        Fecha: 04/11/2020
        Problema Nro 2 Codificado en PHP (Lenguaje con mayor dominio)
    */

    //----- PROGRAMA PRINCIPAL ----- 
    $archivoEntrada = file("entrada.txt");                         //Leemos el archivo de entrada entrada.txt
    file_put_contents("salida.txt", juego($archivoEntrada,0));     //Escribimos el archivo de entrada salida.txt
    //-----FIN  PROGRAMA PRINCIPAL ----- 


    //Funcion juego que realiza el calculo del ganador y cual es la mayor puntuacion
    function juego($archivoEntrada, $mayorPuntuacion)
    {

        for($i=1; $i<= $archivoEntrada[0]; $i++)
        {
            $valor = explode(" ", $archivoEntrada[$i]);
            $res = (int)$valor[0] - (int)$valor[1];
    
            if($res< 0) //gano el jugador  2
            { 
                $ganador = 2;
                $puntuacion = $res * (-1);
            }
            else //gano el jugador  1
            { 
                $ganador = 1;
                $puntuacion = $res;
            }
        
            
            if($puntuacion >= $mayorPuntuacion)  // se va calculando la maxima puntuación
            { 
                $mayorPuntuacion = $puntuacion;
                $gan = $ganador;
            }
        }
        return $gan . " " . $mayorPuntuacion;
    }

?>

