
#Winder Morillo 
#Fecha: 04/11/2020
#Problema Nro 2 - Codificado en Python (Lenguaje que me gustaria aprender)


#Funcion juego que realiza el calculo del ganador y cual es la mayor puntuacion
def juego(archivoEntrada, mayorPuntuacion):

    filas = archivoEntrada.readline()

    for i in range(int(filas)):
        valor = archivoEntrada.readline()
        res = valor.split()
        resultado = int(res[0]) - int(res[1])

        if resultado<0:  #Ganó el jugador 2
            ganador=2
            puntuacion = resultado*-1
        else:            #Ganó el jugador 2
            ganador=1
            puntuacion = resultado
        
        if puntuacion>=mayorPuntuacion: #Se va calculando la máxima puntuación
            mayorPuntuacion = puntuacion
            gan=ganador
            
    return str(gan) + " " + str(mayorPuntuacion)

#Funcion para escribir los resultados en el archivo de salida
def escribirSalida(resultado):
    archivoSalida=open("salida.txt","w")
    archivoSalida.write(resultado)
    archivoSalida.close()
   


#-----INICIO PROGRAMA PRINCIPAL------

archivoEntrada = open('entrada.txt','r')   #Leemos los valores del archivo de entrada
resultado = juego(archivoEntrada,0)        #Llamada a la funcion Juego
archivoEntrada.close()                     #Cerramos el archivo de entrada
escribirSalida(resultado)                  #Llamada a la funcion que genera la salida

#-----FIN PROGRAMA PRINCIPAL------








  




